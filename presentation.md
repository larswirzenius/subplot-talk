title: Subplot
class: animation-fade
layout: true

.bottom-bar[
{{title}}
]

---

count: false

# Leader slide

- Press 'b' to toggle 'blackout mode'
- Press 'p' to toggle 'presenter mode'
- Press 'c' to create a clone of this window which will keep up
- Press left/right to move around slides
- The next slide is where the presentation begins

???

This is where presenter notes appear

---

count: false
title: Formatting examples

.row[

.col-6[

# Header 1

## Header 2

## Header 3

]

.col-6[

Hello world, this text belongs in the second column

]

]

```rust
fn rust_code(from: &str) -> Result<(), ()> {
    todo!()
}
```

---

count: false
class: impact

# {{title}}

## Expressing argumentation in a validatable way

## Daniel Silverstone <br /><tt>&lt;dsilvers@digital-scurf.org&gt;</tt>

???

- Welcome everyone
- Who am I?
- What we'll cover:
  - Safety argumentation documentation
  - Requirements and acceptance criteria
  - Verification criteria as scenarios
  - Turning argumentation into scenarios
  - What is subplot
  - Roundtable? how to apply all of this to safety?

---

class: impact2
count: false

## Disclaimers and thanks

???

- 'Expert' bandied around a little too freely in the world these days
- While I know some of the safety communities lingo etc. not an expert
- I am employed by Codethink, and will use an example of theirs in the talk
- But I am not here for Codethink, and this talk is not sponsored by Codethink
- Thanks are due to a few of my colleagues, Paul A. Shaun M. for helping with safety content
- Thanks also due to my FOSS partner in crime, Lars Wirzenius who is the other half of the Subplot project
  currently.

---

layout: true
title: Safety argumentation documentation

.bottom-bar[
{{title}}
]

---

Section plan

???

- What is safety argumentation, and what form does it usually take?
  - Description
  - Assertion
  - Evidences
- Different kinds of argumentation as we know them.
  - TODO: Check in with Shaun/Paul
- How is argumentation generated, and how is it typically evidenced?
  - Discuss safety requirements, STPA, and process adherence
- Examples of automatic evidencing of argumentation
  - Introduce DCS as an example of automatic evidencing for process adherence
- Validation of automatically evidenced argumentation
  - Discuss how trust derives from certifications, through qualified people, to evidences

documents which include how to assert that the document was verified.
documents which point to other documents which can be verified
those documents can contain verifications for arbitrary parts of the systems

---

# What is safety argumentation

- _Item definition_
- _PHA & RA_
- _Safety Goals_
- _FS Requirements_
- _TS Requirements_
- _Documentation_

???

This should be very familiar to everyone who knows ISO26262.

1. An item, representing a system or function is defined
2. A Preliminary Hazard Analysis & Risk Assessment is done to assign ASIL level
3. Safety goals are derived from this, inheriting the ASIL level
4. Functional Safety Requirements are drawn such that the set of safety goals are met
5. The technical safety requirements are formulated, describing how to implement the FSR
6. Further development includes implementation, integration, and documentation of the safety cases

---

# Can we boil that down a little?

- **Analysis and Description**
- Assertion
- Evidences

???

- All the ISO26262 processes for working out how to validate that a system is safe can be summaried as
  analysis and then description.

---

count: false

# Can we boil that down a little?

- Analysis and Description
- **Assertion**
- Evidences

???

- We can get from those descriptions to some assertions which if met would indicate that the system meets
  the safety case

---

count: false

# Can we boil that down a little?

- Analysis and Description
- Assertion
- **Evidences**

???

- To convince others that the assertions are met, we collate a set of evidences

---

# Doing this in practice

- **Analysis**
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST

???

- Various possible ways to analyse a system from the point of view of trying to understand its safety properties

---

count: false

# Doing this in practice

- Analysis
  - **FMEA / FMECA**
  - Fault Tree
  - STAMP/STPA/CAST
- Description
- Assertions
- Evidences

???

- Failure Mode and Effects Analysis
- Failure Mode, Effects, and Criticality Analysis
- Commonly used approach which uses inductive reasoning to synthesise a full system view from the bottom up
- Originated in the US minitary in the 40s, it's a solid and well understood approach to understanding the risks in a system
- Good for exhaustively cataloging faults and local effects

---

count: false

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - **Fault Tree**
  - STAMP/STPA/CAST
- Description
- Assertions
- Evidences

???

- Fault tree analysis
- Developed in Bell Labs in the 60s _for_ the US military
- Top-down analytical approach, uses common boolean logic combinators to express the analysis
- Good for understanding how a system responds to multiple simultaneous faults but not at finding all possible faults
- Commonly used together with FMEA/FMECA

---

count: false

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - **STAMP/STPA/CAST**
- Description
- Assertions
- Evidences

???

- Systems-Theoretic Accident Model and Processes
- Systems Theoretic Process Analysis
- Causal Analysis based on STAMP
- Much newer approaches first described in Nancy Leveson's 2021 book "Engineering a Safer World"
- Models systems as components/processes, control actions, and feedback pathways
- Can only identify hazards at the level of the analysis
- But can drill into system components to whatever depth is necessary to identify all hazards

---

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST
- **Description**
- Assertions
- Evidences

???

- Having run the various analyses we've just described, you are left with a number of documents which
  explain how a system might fail, and to a greater or lesser extent, how that might affect the system.
- The exact format for writing up these hazard conditions and effects will vary from approach to
  approach, as well as from company to company; however no matter what, the description will cover
  the hazard conditions, the likelihoods, the criticality of the failure, and so on.
- Commonly the description must be comprehensible to the people ultimately responsible for confirming
  the safety properties of the system. This might be for certification, Quality Assurance, or any
  other reason.
- Also the description has to be comprehensible to the people responsible for formulating the assertions
  for the system.

---

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST
- Description
- **Assertions**
- Evidences

???

- The assertions for a safety argument are the statements which, if met, the author of the assertions
  believe will evidence the safety properties of the system.
- Not all assertions will be directly related to things traditionally thought of as safety though.
- For example, you might have determined in your analysis that a hazard could be that the compiler
  sometimes, unpredictably, produces bad machine code for a given good source code input.
- You might make an assertion that for any given code input the compiler will _always_ produce the
  same machine code output, permitting a later assertion that the machine code does the right thing
  to rely on the fact that it will never cease doing the right thing just because of the compiler.

---

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST
- Description
- Assertions
- **Evidences**

???

- If everyone reading the analysis, description, and assertions, agrees that with the assertions being
  met the system safety properties are sufficient, then the last step in the argumentation is to gather
  and provide evidences
- Evidences are typically gathered together by a person, to the extent necessary to demonstrate that the
  assertions are met. Then by simple inspection of the collated documentation, the properties of the
  system can be confirmed.
- Sadly this does not (a) scale to large systems without having large numbers of people working on this,
  or (b) scale to a continuously updating system without having to continuously re-do this work.
- Modern systems which have to be safe are also more and more likely to contain significant quantities of
  computer software which in an always-on always-connected world implies semi-continuous updates etc.

---

class: impact

## Deterministic Software Construction

???

- Codethink spoke toward the end of last year, at the ELISA workshop, about their
  deterministic software construction service and how it achieved ISO29292 certification.
- We'll use this as an example of how we can start to bridge the gap between traditional
  safety argumentation analysis and evidencing, and the well trodden paths of good
  software engineering practice

---

count: false

# Deterministic Software Construction

- **A design pattern for reproducible software construction**
- A way to verify this reproducibility for a given set of inputs in a given DCS instantiation
- Using these properties to inform verification and impact analysis
- Automating all of this into a continuous integration workflow

???

- I mentioned earlier the idea that one part of determining that software is safe could be asserting
  that for the same inputs you always get the same outputs.
- This idea is well known in the open source software world, and is referred to as reproducible
  builds, or bit-for-bit reproducibility

---

count: false

# Deterministic Software Construction

- A design pattern for reproducible software construction
- **A way to verify this reproducibility for a given set of inputs in a given DCS instantiation**
- Using these properties to inform verification and impact analysis
- Automating all of this into a continuous integration workflow

???

- Naturally when we think of reproducibility in software we have to take into account not
  only the traditionally thought of inputs (source code, libraries, etc) but also the
  wider context in which software systems are constructed.
- While it sounds quite weasel-wordy, DCS demystifies this by simply treating the DCS instance
  as part of the inputs, covering not only the environment for the construction, but also
  the rules and processes governing it.

---

count: false

# Deterministic Software Construction

- A design pattern for reproducible software construction
- A way to verify this reproducibility for a given set of inputs in a given DCS instantiation
- **Using these properties to inform verification and impact analysis**
- Automating all of this into a continuous integration workflow

???

- In a trivial sense, if a chance in inputs has no effect on the constructed software
  artifacts, then it has no impact on the behaviour or other properties of the system.
- In this way, a basic check for "did this bug fix actually affect the output" can help
  to evidence if work was of value.
- Ditto, if a change in one component does not affect any other component, then there can
  be a reasonable reduction in total system testing required as a result.

---

count: false

# Deterministic Software Construction

- A design pattern for reproducible software construction
- A way to verify this reproducibility for a given set of inputs in a given DCS instantiation
- Using these properties to inform verification and impact analysis
- **Automating all of this into a continuous integration workflow**

???

- Again, as I mentioned earlier, modern software systems change a lot, perhaps even all the
  time.
- By integrating all the validation and impact analysis steps into a continuous workflow
  you can be more confident that changes your developers are making do not adversely affect
  safety properties of the system.

---

class: impact
count: false

### Deterministic Software Construction

# Necessary, but not sufficient.

???

- All of the above is a necessary part of getting toward making automated safety assertions
  about software systems
- However at the moment, DCS cannot make any assertions about the safety properties of the
  software built using it
- Instead it provides the first step - confidence that if we do analyse the software systems
  and make assertions, that nothing about the processes of converting that to production
  software could invalidate those assertions.

---

# Automatic evidencing of argumentation

## Deriving trust

- **What can DCS do now?**
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidences

???

- So DCS can provide us with a process of generating automatic evidences which go alongside
  the argumentation DCS makes about the process of software build and integration.
- The evidences can be automatically constructed and thus the human portion of aggregating
  evidences and collating the argumentation can be eliminated, saving time, effort, and
  reducing risk.

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- **How does that play into validation?**
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidences

???

- In the safety world, we need to establish a sequence of trust from established norms
  through to the system in production
- This chain involves certifications, qualified people, systems, and processes,
  argumentation, and the evidences supporting that argumentation

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - **Standards and certification**
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidences

???

- Ultimately, trust starts in consensus around standards such as ISO26262

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - **Qualified people, systems, and processes.**
  - Argumentation
  - Evidences

???

- From there, we look to certify a combination of people and processes
  such that we can draw a line of trust from the standard through those people and processes

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - **Argumentation**
  - Evidences

???

- Those people and processes are then entrusted to produce analyses
- From there, they can describe the safety properties of systems, identify hazard conditions etc.
- They (both people and processes) then produce/provide argumentation around those properties and conditions
- The assertions that are contained in the argumentation have trust derived from the creators of them.

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - **Evidences**

???

- Finally, either by automation from the processes, or by dint of work done by qualified people
  we can construct evidences to support the argumentation
- As someone outside of the process, we can derive a level of trust that the evidences show
  the safety properties of the system by following the trust chain.
- We trust that the safety standard is good and if followed will result in a safe system
- We trust that the qualified people, systems, and processes meet that standard and that they
  will do the "right thing" whatever that may need to be
- We trust that the constructed argumentation was done in conjunction with those trusted people
  systems and processes
- And we trust that the evidences generated support the argumentation by dint of trusting
  the whole chain to tell us if they didn't.

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidences

## Turtles all the way down

???

- If your high level documents (standards) tell you how to know if the next level of documents
  are good, and those documents tell you how to know if the next level is good etc.
- Then you can chase this trust all the way down from the standards to the individual
  components (software or otherwise) in a system
- And then you can chase the evidences back up the trust chain, verifying at each level, until
  eventually you can determine your trust in the system in question

---

layout: true
title: Requirements and acceptance criteria

.bottom-bar[
{{title}}
]

---

Section plan

???

- What are requirements?
  - Functional vs. non-functional
  - High level vs. low level
- Validating requirements (testing)
  - Validation by automated criterion
  - Validation by inspection
  - Validation by analysis
- Stakeholders in requirements validation
  - Everyone from the CEO of the product company through to individual programmers
- What are acceptance criteria
  - Kind of requirement
  - Often very high level
  - Usually defined by high level stakeholders, or at least approved by them
  - Can still be validated automatically sometimes

---

# What are requirements?

- High level vs. low level
- Functional vs. non-functional

???

- Requirements are statements about how a system must or must not behave
- At a high level these might be business need statements, and at a low level
  they may state exactly the angle of a screw thread, or a singular behaviour
  or property of a software component.
- While the term applies across the full scale, typically requirements are
  used in a formal sense only in engineering design, and we use different words
  for the higher levels of system specifications
- Functional requirements specify capabilities, behaviours, and information that
  any solution must have or exhibit in order to to be effective etc.
- Non-functional requirements specify properties, constraints, or characteristics
  of solutions. For example, reliability, availability, etc.

---

# Validating requirements

- **Analysis**
- Inspection
- Demonstration
- Automated criterion

???

- The act of determining the requirements of a system is often called requirements
  analysis. This starts from high level requirements and then breaks those down into
  further requirements.
- The high level requirement is then said to be met if all the broken down requirements
  are met by the system
- We call this 'Validation by analysis' and this forms the backbone of a lot of requirements
  validation in use today.

---

count: false

# Validating requirements

- Analysis
- **Inspection**
- Demonstration
- Automated criterion

???

- Sometimes a requirement can only be verified met by means of inspecting something
- For example, a requirement that an interface be aesthetically pleasing could perhaps
  be verified by means of some trusted party looking at the interface and making a statement
  about its aesthetics.
- This doesn't _have_ to involve exercising the system under test if the inspection is of
  some other part of the system or processes.

---

count: false

# Validating requirements

- Analysis
- nspection
- **Demonstration**
- Automated criterion

???

- Sometimes called 'human testing'
- Verification by demonstration involves using the system as intended and showing that
  the properties of the running system match the requirements set out.
- An example here might be showing that operating an electric window switch in certain
  conditions (e.g. the ignition switch being on) results in the window moving as expected.
- Sometimes this can be quite detailed, involving controlled conditions etc. at which
  point this may be referred to as 'Test' rather than demonstration

---

count: false

# Validating requirements

- Analysis
- nspection
- Demonstration
- **Automated criterion**

???

- Automated criteria for validating requirements are often referred to as 'test cases'
- These can be executed automatically by some kind of testing system or harness
- Very common in software systems, commonly simply called 'tests' in that scenario.
- Variety of kinds of tests, including:
  - unit tests
  - integration tests (various levels, but open box)
  - system tests (kind of integration, but treated as a closed box)

---

class: impact

# Stakeholders

## Who cares about validation?

???

- It's reasonable, when considering requirements and validation, to think about who
  in particular cares about each requirement.
- The particular stakeholders who can be said to have an interest in any given requirement
  may dictate the manner in which that requirement is validated.
- Let's take a walk through the stakeholders of a software product
- In our example we're going to exaggerate the distance between the understanding
  of someone on the board of a company, and the individual programmer writing code.
- The actors in this walkthrough are very self-absorbed and find it hard to look
  outside of their particular bubble. This is not completely realistic but let's
  us consider different points of view.

---

# Stakeholders in validation

- **Company board, CEO**
- Product owner
- System architect
- Development teams
- Individual engineers

???

- While it may sound odd, just like trust chains start at the very top, so does
  the care about requirements.
- The board of a company, the CEO/CTO/etc. must have some level of stake in the
  requirements of a product their company is producing. Their stake is likely
  at the level of business needs, but they will need to know that the needs
  will be met by the product.

---

count: false

# Stakeholders in validation

- Company board, CEO
- **Product owner**
- System architect
- Development teams
- Individual engineers

???

- The owner of a product is responsible for its definition and success. They
  are responsible for ensuring that the product design etc. meets the business
  needs and that the mechanisms of validation are understood by the board etc.
- The product owner is also responsible for ensuring that the system architects
  understand the requirements well enough to analyse the system and define
  requirements further.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- **System architect**
- Development teams
- Individual engineers

???

- System architects are often the first point at which non-functional requirements
  and very high level functional requirements begin to be broken down into requirements
  which stand a chance of automated validation
- Though any requirement at any level _could_ be automatically validated
- A system architect may not need to understand how the product might meet the business
  needs of the company, but they will have had those effectively broken down and
  communicated as product requirements by the product owner.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- System architect
- **Development teams**
- Individual engineers

???

- Development teams receive requirements defined by the system architect and through
  analysis and design, determine how to build software which meets those requirements.
- They likely understand the full product at some level, but may only be responsible
  for building some sub-part of it.
- This is likely the point at which requirements begin to take on quite technical
  forms, tightly defining individual component behaviours.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- System architect
- Development teams
- **Individual engineers**

???

- The individual engineer is likely working from well defined low level requirements
- They may have to implement particular software components to specific standards
  protocols etc.
- In an extreme sense, they may have no concept of how that component fits into the
  product as a whole. This is quite common when software component development is
  outsourced.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- System architect
- Development teams
- Individual engineers

## Comprehension goes one up/one down

???

- As a result, when we look at this heirarchy of responsibility and understanding
  we see that comprehension of requirements can go one level up or down only.
- i.e. the product owner needs to comprehend the board's needs in order to present
  requirements to the system architect
- but they will also need to comprehend the system architect's evidences in order to
  be satisfied that the requirements they passed down are met, so that they can then
  generate evidences for the board above them.
- All these levels could seem very onerous, and in some cases they really can be.
- In reality, while the difference between the levels may be less pronounced than in
  our exaggerated example world, the heirarchy will not usually be so clear-cut and linear,
  especially when you get other departments involved in system design, for example
  a cybersecurity department may place requirements on a system with no regard to
  the product's functionality per-se, but in order to meet some business needs around
  not producing insecure software.

---

class: impact

## Acceptance criteria

## and

## Verification critera

???

- Another term you'll hear bandied around is 'acceptance criteria'
- In some sense, particularly for our purposes, this is just another word for requirement
- Typically high level, often from the perspective of a user of the system
- Usually defined by very high level stakeholders, or at least approved by them
- Can still be automatically validated, at least sometimes.
- Usually coupled with 'verification criteria' which is a description of how the
  acceptance criterion is verified to have been met. Effectively a 'test case'.

---

layout: true
title: Verification criteria as scenarios

.bottom-bar[
{{title}}
]

---

Section plan

???

- Kinds of testing people are familiar with
  - Unit testing
  - Integration testing
  - System/black-box testing
  - Verification by inspection (user testing)
- Popularity of Agile
- User stories as a way to break work down
  - Oftentimes these come out like: "As _persona_ I _do this_ such that _this happens_
- Codifying that approach into testing produced the Gherkin language
  - Given, When, Then
- The goal being to render verification criteria in a way where stakeholders can comprehend them
  - Ideally when the test owner writes a test, their "upstream" requirements holder can trivially
    agree that the test will validate the requirement; and the programmer can easily implement each
    step of the test such that the test owner can agree the code matches the intent of the scenario
  - This is critical because test code is often not directly tested itself, and as such it needs
    to be the best, cleanest, clearest, and most well documented of the code in a project

---

class: impact

## Verification criteria

???

- Let's take a moment to focus on verification criteria
- I will focus pretty much only on software, but in theory
  all of this can apply in some sense to hardware systems
  or processes etc.

---

# Kinds of testing

- Unit testing
- Integration testing (open-box)
- System/closed-box testing
- Inspection

???

- Unit testing treats individual isolated components of a system as a unit of functionality
  and verify their behaviour. In the extreme this may involve calling individual software
  functions to verify they behave as intended.
- When the interaction between units needs to be verified, we move to integration testing.
  We refer to this as open-box because typically the testing harness will have some privileged
  access to internal state of software components in order to do this verification.
- System, or closed-box testing, is a higher level variant on integration testing, which verify
  the behaviour of an integrated aggregate of components without privileged access to their
  internal states. Verification is done by observing (in an automated fashion) the behaviour
  of the system as a whole
- Verification by inspection, or user testing, is still necessary in some cases, though it is
  much harder to automate.

---

class: impact

## The rise of agile methodologies

### User stories

???

- While what I've spoken about so far is quite traditional, more and more software teams are
  starting to use a variety of agile methodologies including capital-A Agile, Lean, etc.
- The big thing which came out of agile which we are interested in today are user stories.
- Often presented in the form Persona - need - purpose
- e.g. "As a visitor, I want to buy a ticket, so that I can watch a film"

---

class: impact2

## Cool as a cucumber

# Given, When, Then

???

- Codifying the user-story approach to expressing requirements led to the creation of
  the Gherkin language
- This is a standard for expressing verification criteria in a scenario form, enabling
  the reader of the criterion to understand the setup, action, and checks which comprise
  the validation of the requirement.

---

## As a visitor, I want to buy a ticket, so that I can watch a film.

```
given a cinema booking system
and an empty shopping cart
when the user chooses a film
then the shopping cart contains a ticket for the film
when the user selects checkout
then the ticket is sold
```

???

- Scenarios describe the setup of the situation (given)
- Actions which are performed (when)
- and verifications of the results (then)
- Each step in the scenario can be customised to the system being verified
- Typically steps are designed to be generic enough that more criteria can be
  written using a combination of existing steps, rather than defining unique
  steps each time

---

## As a stakeholder, when a scenario exists, then I can understand it

- **Looking "upward"**
- Looking "down"
- Test code is the best code

???

- When a test owner writes a scenario for meeting a requirement, it should be
  clear to the author of the requirement what the scenario needs/assumes, what
  it will do, and how it will check the result of that.
- This clarity ensures that stakeholders further "up" the chain can comprehend
  tests and accept that they will validate their requirements, without actually
  needing to understand how they will do that in detail

---

count: false

## As a stakeholder, when a scenario exists, then I can understand it

- Looking "upward"
- **Looking "down"**
- Test code is the best code

???

- The test owner write scenarios using steps which are defined in conjunction with
  the people responsible for actually implementing the test.
- This means that the test owner can trust that the programmer will understand
  the purpose of the steps, and they can both agree that the code to implement
  the step matches the intent of the scenario
- Nominally it also means that the programmer need not understand the wider product
  codebase fully, nor the requirements set, to be effective at their job.

---

count: false

## As a stakeholder, when a scenario exists, then I can understand it

- Looking "upward"
- Looking "down"
- **Test code is the best code**

???

- Some years ago, I wrote a set of "trusims" of software.
- Two of them were: "If you don't know why you're doing it, you shouldn't be doing it."
  and "If it's not tested, it doesn't work"
- The former is a restatement of the idea that software products should only contain
  code needed to meet requirements of the system. If there's no requirement for code
  to exist, it shouldn't.
- The latter then covers the idea that if you wrote the code, you'd best be certain that
  it works, and the best way to do that is to have a test for that feature.
- Sadly, test code is often only verified to work by dint of running it against a system
  and then making a decision on test failure as to whether the test is bad, or the system
  under test is at fault.
- Given that, your test code needs to be patently clear and correct such that it can be
  validated by inspection.
- The scenario approach to testing permits the higher level test to be validated independently
  of the code implementing the test, meaning that more people can be confident of the correctness.

---

layout: true
title: Turning argumentation into scenarios

.bottom-bar[
{{title}}
]

---

Section plan

???

- What would be the value in rendering argumentation as scenarios?
  - Automatic validation
  - Continuous compliance checking
  - Ensures the minimum of assertions are validated purely by analysis/inspection
- Converting some examples
  - Use examples from DCS
  - Explain what the example argumentation is about, and how the evidences show this
  - Show how that high level understanding lets us write a scenario/scenarios
- Now consider each step in turn and think about the active operation which will occur
  - For each step we defined before, write out a pseudocode implementation of the step
- Now that, in theory, things are automatic, what value does this bring from various perspectives?
  - CISO/CTO/CEO for high level scenario
  - LL sys-eng for low-level scenario
  - Now reverse, can either gain value the other way around?

---

count: false
class: impact

# Argumentation as scenarios

???

- Let's take a look at the benefits of rendering argumentation as scenarios,
- how we might do that
- and what different perspectives might see as the value it brings

---

# Rendering argumentation as scenarios

- **Automatic validation**
- Continuous compliance checking
- Reduction in validation by analysis/inspection

???

- The biggest and most obvious value, from my perspective at least, is that the validation
  can be automated.
- Critically this is not suggesting that scenarios result in automatic construction of argumentation
  but rather that creating and matching evidences to assertions could be automatic
- Anything which is automated has a lower risk of human error and can be fitted into modern
  system construction pipelines.

---

count: false

# Rendering argumentation as scenarios

- Automatic validation
- **Continuous compliance checking**
- Reduction in validation by analysis/inspection

???

- Make your scenarios about ensuring that processes are followed properly, and the evidences
  produced by those scenarios supporting the assertions for that, and you get something great.
- Automatic execution of scenarios which confirm that processes, systems, and activity is
  complying with standards is the core of ensuring that any modern software project could
  be safe.

---

count: false

# Rendering argumentation as scenarios

- Automatic validation
- Continuous compliance checking
- **Reduction in validation by analysis/inspection**

???

- Again, from the perspective of "Humans are fallible" if a computer can take the carefully
  constructed scenarios and by executing them provide confidence that each level of a system's
  trust model is correctly being handled, there is a significant reduction in the human cost
  to making safe systems.
- While it doesn't mitigate all the analysis, since clearly some will have to happen in order
  to (a) create the sub-level and (b) satisfy the stakeholders that if the scenario passes then
  the assertions are met, it does reduce the complexity of the analysis.
- No longer will someone checking if a process is being followed need to inspect large amounts of
  evidence to verify that they support an assertion, instead they inspect the scenario, rely on
  the trust chains to be satisfied that the evidences will be useful, and then take the outputs of
  the automated process.

---

# TODO: Render a DCS argument as a scenario, or else something from ELISA?

# TODO: Ideally one very high level scenario, one low level scenario

# TODO: Get help from Paul A. or Shaun M.

---

# TODO: Pseudocode implementations of the steps for the above scenario

---

# Perspectives in deriving value

- **CISO/CTO/CEO**
- Low level systems engineer
- Turnabout is fair game?

???

- TODO: Talk about how someone high up in a firm derives value from the high level scenario

---

count: false

# Perspectives in deriving value

- CISO/CTO/CEO
- **Low level systems engineer**
- Turnabout is fair game?

???

- TODO: Talk about how someone low down derives value from the low level scenario

---

count: false

# Perspectives in deriving value

- CISO/CTO/CEO
- Low level systems engineer
- **Turnabout is fair game?**

???

- TODO: Talk about how the value is still there, but in a very limited fashion, when we reverse the roles/scenarios

---

layout: true
title: What is subplot

.bottom-bar[
{{title}}
]

---

Section plan

???

- Background of Subplot project (yarn etc)
  - Three friends in a restaurant chatting over lunch nine years ago
  - Initial implementation as a testing tool only
  - Spent years experimenting with this approach, producing good test suites but poor documentation
  - Five years ago, two friends sat in front of a whiteboard and discussed what we wanted yarn to be longer term
  - Spent two and a bit years on and off experimenting with these bits. More complex language features, etc.
  - Late 2019, decided that simplicity was key, and that documentation was the primary goal, thus pivoted to Subplot
  - Goal of Subplot (high quality documentation, automatic testing as secondary feature)
  - Six months to something usable, another year to first alpha, now approaching second alpha release as we learn more
    through using the tool
- Describe featureset of Subplot
  - Start with the docgen features (diagrams, PDF, HTML, etc)
  - Then talk about what codegen is
  - Flexibility in templating.
- Bootstrapping principle (subplot tested with subplot)
- Explanation of current state of Subplot
  - Still alpha software
  - Seeking interested parties who want to help the project's direction or development
  - FOSS, on gitlab
  - Bash, Python, Rust as code targets right now
- Invitation to have a go, and take part in the project

---

class: impact

## Long ago, in a galaxy far far away…

???

- Approximately ten years ago, three friends, in a restaurant, were chatting over their lunch.
- They were discussing how unpleasant it was to write the tests for a tool they were working on.
- One of them had heard about Gherkin and scenarios and they liked the idea
- Unfortunately at the time, the only implementations they knew about were either in Ruby
  (which wasn't in use on the project) or else were aimed at web applications (which wasn't what
  our intrepid friends were developing)
- They set out to write and use a simple scenario testing tool which they called `yarn`
- Over the next few years, they wrote a lot of good test suites for a variety of software,
  proving out that the concept of scenario testing worked for UNIX CLI tools
- Sadly the mostly produced bad documentation along the way as well, as is the wont of many
  software engineers.

---

class: impact

## Some time ago, but this time in our galaxy…

???

- Five years ago, two of those friends sat in front of a whiteboard and discussed what they wanted
  `yarn` to be long term, thinking about clever features, and ways to do more complex testing.
- They spent the next two years, on and off, experimenting with these ideas; writing more good
  test suites, and still producing awful documentation.
- Late in 2019, they performed some basic user surveying, decided that simplicity was key,
  documentation was the most valuable / primary goal, and thus they pivoted to the ideas which became Subplot.
- They set the goals of Subplot (high quality documentation generation, with automatic testing as a secondary feature)
- Six months saw them produce something usable in projects other than Subplot
- Another year got them to the first alpha version
- Now approaching the second alpha release as we learn more through using the tool

---

# Some features of Subplot

- **Documentation**
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Subplot's initial, and nominally primary, purpose is that of documentation
- Subplot documents are exactly that, documents, likely primary composed of
  prose, with images and scenarios as appropriate.
- Subplot documents are meant to be revision controlled, for example in a
  Git repository, and so are plain text documents in terms of inputs to Subplot's
  document generation feature.

---

count: false

# Some features of Subplot

- _Documentation_
  - **Markdown text**
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Like Yarn, and countless other tools, Subplot uses Markdown as its input language.
  Nominally this could have been asciidoc or reStructured text, but the authors of
  Subplot are most comfortable with Markdown
- Markdown documents are fairly easy to read in their source form because rather than
  tags like HTML, Markdown's formatting looks approximately like you'd expect the output
  text to look.

---

count: false

# Some features of Subplot

- _Documentation_
  - Markdown text
  - **Diagrams as first class content**
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Not everyone is satisfied purely with words. Sometimes a diagram can be much
  easier to understand.
- Markdown supports arbitrary embedding of images.
- However Subplot supports a number of textual diagram formats already, and more
  could be added.
- Currently Subplot supports the use of PlantUML for any of its UML diagram forms
- Subplot also supports `pikchr` which is a graph like language which describes
  flowchart type diagrams
- Subplot supports `dot` which is a more generic graph like language often used
  to show simple relationships, `dot` actually underlies PlantUML's output I believe.
- Each of these formats allows document authors to more easily revision control their
  diagrams and understand differences from version to version.

---

count: false

# Some features of Subplot

- _Documentation_
  - Markdown text
  - Diagrams as first class content
  - **PDF generation for archival**
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Since oftentimes you need to archive your argumentation as part of your release process,
  or to support later inspection by auditors, Subplot offers PDF rendering.
- This means that auditors need not understand Markdown nor the text forms of the diagrams,
  instead being able to view a fully rendered document.
- We are aware of some users of Subplot who only use it for rendering documents because
  they like the Markdown and diagram-as-text support

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - **HTML generation for ease of reading/flexible consumption**
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Even more widely supported as a document format is HTML.
- Subplot can render your documents as stand-alone HTML which can then be published
  onto websites, into document management systems, or simply stored for viewing without
  need for PDF software.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- **Code generation**
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- If we were only talking about a tool for nicely rendering documentation
  then we could stop here, but Subplot has a second major function.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- _Code generation_
  - **Generate your test suite**
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Unlike many tools, including Yarn, Subplot does not act as a test
  runner or orchestrator.
- Instead, Subplot can be used to _generate_ source code which runs
  the tests.
- Subplot can convert all the scenarios in a document into code which
  can run under more traditional test runners in order to be maximally
  flexible.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- _Code generation_
  - Generate your test suite
  - **Step libraries**
  - Flexible templating
- Bootstrapping principle

???

- To permit Subplot to actually understand and generate appropriate
  code, your document will list some number of step libraries in its
  metadata.
- Subplot uses those step libraries, through a data model we call
  bindings, to determine how to turn the scenarios into code to run.
- Subplot actually provides a number of standardised step libraries,
  including those which can run commands, read and write files, and
  a variety of other features.
- You can, and indeed likely will need to, write step libraries for your
  projects. They may vary in complexity and implementation "level"
  as needed to fulfil your needs for verifying your requirements.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- _Code generation_
  - Generate your test suite
  - Step libraries
  - **Flexible templating**
- Bootstrapping principle

???

- Finally, Subplot's code generation is templated and fairly flexible.
- We provide, as standard, a Python test runner which is very basic,
  but also very easy to use.
- The next release of Subplot will include support for generating Rust
  test suites which fit directly into Rust's testing model, making it
  easy to integrate Subplot scenarios into testing Rust code.
- We provide a basic shell (bash) based test suite template as well,
  though that is not as fully featured nor fully supported.
- As with step libraries, projects can provide their own templates,
  for example if you are verifying a component written in Typescript,
  or Ruby, you might provide a template which generates tests consumable
  by appropriate test runners for those languages.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- **Bootstrapping principle**

???

- You may have heard the phrase "Eat your own dogfood"
- In this instance I'd prefer to say "Use your own tools"
- Subplot's primary set of verification critera are written as
  scenarios in Subplot's own documentation.
- This means that we use Subplot to test Subplot meets its requirements.
- Interestingly, we do this in all languages/test-suite styles that Subplot
  supports, where appropriate.
- We feel that this principle is important since it shows real-world
  use of the technique, and while I'll be the first to admit that we're
  not yet great at writing the documents ourselves, we're learning and
  improving all the time, through forced use of the technology for ourselves.

---

# The state of the Subplot project

- Still Alpha software
- Actively seeking interested parties
  - To help directing the project
  - To help developing the project
- Free software, hosted on GitLab
- Uses other free software projects
- Open decision making processes
- Written in Rust, with Bash, Python, and Rust code targets

???

- In summary, Subplot, despite being many years since that fateful lunch,
  is what I'd describe as alpha-grade software
- Currently run by two people - myself and Lars Wirzenius
- We have had contributions from some of our friends, including the third
  person at the original inception lunch
- We are painfully aware that two people cannot possibly represent the full
  gamut of possible users, nor use-cases. If this sounds interesting, we'd
  love to chat with you about how you can help.

---

count: false

- Still Alpha software
- Actively seeking interested parties
  - To help directing the project
  - To help developing the project
- Free software, hosted on GitLab
- Uses other free software projects
- Open decision making processes
- Written in Rust, with Bash, Python, and Rust code targets

???

- Subplot is under the MIT licence to make it as easy as possible to integrate
  with both open and closed source workflows
- Rather than re-implementing the world, Subplot uses Pandoc for rendering the
  PDFs and HTML, PlantUML, dot, and pikchr for diagramming, and any number of
  open source libraries to achieve its goals.
- The project's decision making process consists of fortnightly meetings where
  issues are discussed, designs are drawn up, etc. These meetings are minuted
  on the Subplot website. Larger design goals are tackled one at a time, and
  we are currently working on our second such goal, the bringing of our Rust
  template up to par in order that we can consider it supported by default.
- Subplot is written in Rust, though as I mentioned, we have a number of
  language test-suite templates built into the tool, the most mature of which
  is the Python one.
- We'd love for you to come and join us and help Subplot grow to be more useful
  for the safety community.

---

layout: true
title: Subplot - Expressing argumentation in a validatable way

.bottom-bar[
{{title}}
]

---

class: impact

## Roundtable discussion idea:

## Using scenarios in safety argumentation

???

- Perhaps when we're through with any questions any of you have about Subplot,
  we might have a discussion in the channel about safety argumentation and
  automated validation of assertions.
- Do any of you think Subplot, or a similar tool, might have a place in making
  safety argumentation validation a more continuous process?

---

class: impact

# Any questions

## <https://gitlab.com/subplot/subplot>

## <https://subplot.liw.fi/>

???

- Until then…
- Does anyone have any questions?
